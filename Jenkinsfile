@Library('jenkins-lib') _

pipeline {
  options {
    gitLabConnection('default')
  }

  agent {
    kubernetes {
      inheritFrom 'build_amd64'
    }
  }

  stages {
    stage('Set the environment') {
      steps {
        updateGitlabCommitStatus name: 'build', state: 'running'
        script {
          env.GIT_COMMIT_MSG = sh(script: 'git log -1', returnStdout: true)

          def Map variables = readProperties file: '.env'
          variables.each { key, value ->
            //might conflict with existing variables and throw an exception
            env."${key}" = value
          }
        }
      }
    }
    stage('Build the image') {
      steps {
        container('buildkit') {
          sh """
            buildctl \
              --addr tcp://buildkitd.default.svc:1234 \
              --tlscacert /buildkit_client_cert/ca.pem \
              --tlscert /buildkit_client_cert/cert.pem \
              --tlskey /buildkit_client_cert/key.pem \
              build --frontend dockerfile.v0 \
              --local context=./ \
              --local dockerfile=./ \
              --output type=docker,dest=image.tar \
              --export-cache type=registry,ref=${OWN_REGISTRY} \
              --import-cache type=registry,ref=${OWN_REGISTRY}
          """
        }
      }
    }
    stage('Push the image') {
      steps {
        container('crane') {
          sh """crane push ./image.tar "${OWN_REGISTRY}:${GIT_BRANCH}-${OWN_VERSION}" """
        }
      }
    }
    stage('Deploy') {
      /*when {
        expression {
          return "${GIT_BRANCH}" != "master"
        }
      }*/
      steps {
        container('helm') {
          sh 'ls -lh /var/run/secrets/kubernetes.io/serviceaccount'
          sh '''#!/usr/bin/env bash
            set -eu

            cd /var/run/secrets/kubernetes.io/serviceaccount
            kubectl config set-cluster cfc --server=https://kubernetes.default --certificate-authority=./ca.crt
            kubectl config set-context cfc --cluster=cfc
            kubectl config set-credentials user --token=$(< ./token)
            kubectl config set-context cfc --user=user
            kubectl config use-context cfc

            OUTPUT=$(helm template "discord-bot-${GIT_BRANCH}" \
              --set image.tag=${GIT_BRANCH}-${OWN_VERSION} \
              $(if [[ "$GIT_BRANCH" != 'master' ]]; then \
                echo "--set PREFIX=$GIT_BRANCH"; \
                echo "--set spec.resources.limits.cpu=1"; \
              fi) \
              --set spec.secretMounts.discord-token-${GIT_BRANCH}.mountPath=/var/secret \
              --set spec.secretMounts.discord-token-${GIT_BRANCH}.key=token \
              --set spec.secretMounts.discord-token-${GIT_BRANCH}.readOnly=true \
              --set ramdisk=/tmp \
              "${WORKSPACE}/deploy")
            echo "$OUTPUT"
            echo "$OUTPUT" | kubectl apply -f -

            if echo "$GIT_COMMIT_MSG" | grep '{FORCE_REDEPLOY}' >/dev/null; then
              echo 'Rollout restart of the deployment (found {FORCE_REDEPLOY} in the commit message)'
              kubectl rollout restart deployment "discord-bot-${GIT_BRANCH}"
            fi
          '''
        }
      }
    }
  }

  post {
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
    unsuccessful {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    aborted {
      updateGitlabCommitStatus name: 'build', state: 'canceled'
    }
  }
}
