#!/usr/bin/env bash

while getopts 'i:t:s:o:eh' opt; do
  case "$opt" in
    i)
      INPUT_FILE="$OPTARG"
      ;;

    t)
      TEXT="$OPTARG"
      ;;

    s)
      DELAY_S="$OPTARG"
      DELAY_MS=$(bc <<< "scale=0; ($3*1000)/1")
      ;;

    e)
      echo "-e Not implemented"
      exit 1
      ;;

    o)
      OUTPUT_FILE="$OPTARG"
      ;;

    ?|h)
      echo -e "-i\tinput video file or '-' if  to be taken from stdin\n\
\t-t\ttext-to-speech text\n\
\t-s\ttts insert timestamp [s]\n\
\t-e\t[optional] tts end timestamp [s], will shrink tts audio to fit (not implemented)\n\
\t-o\toutput filepath or '-' if to be read from stdout\n\
\t-h\tthis text"
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

TMP_DIR=/tmp/rememe
mkdir -p $TMP_DIR

WAV_FILE="${TMP_DIR}/$(echo "$(date +%s) $(date +%N)" | md5sum | cut -d ' ' -f 1).wav"
espeak -p50 "$TEXT" -w "$WAV_FILE"

trap "rm '$WAV_FILE'" exit

FILE_DURATION_S=$(ffprobe -i "$WAV_FILE" -show_entries format=duration -v quiet -of csv="p=0")

INSERT_END_TIMESTAMP_S=$(bc <<< "${DELAY_S}+${FILE_DURATION_S}")

set -x

ffmpeg -i "$INPUT_FILE" -i "$WAV_FILE" -filter_complex \
  "[1]adelay=${DELAY_MS}|${DELAY_MS}[delayed]; \
  [0:a]volume=enable='between(t,${DELAY_S},${INSERT_END_TIMESTAMP_S})':volume=0[amute]; \
  [amute][delayed]amix=inputs=2:duration=longest:dropout_transition=2" \
  -c:v copy -c:a aac -b:a 256k -f mp4 "$OUTPUT_FILE"