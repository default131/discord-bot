FROM python:3.12-slim-bullseye as workspace

ARG USERNAME=botuser
#render group id
ARG USER_UID=106
ARG USER_GID=$USER_UID
ENV PREFIX=uwu

RUN groupadd --gid $USER_GID $USERNAME

RUN useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

WORKDIR bot

RUN chown $USER_UID:$USER_GID ./

FROM workspace as pip

COPY requirements.txt ./

USER $USERNAME

RUN pip install --no-cache-dir -r requirements.txt --user

FROM workspace as piper

USER $USERNAME

WORKDIR piper

COPY piper ./

RUN tar -xvf piper_amd64.tar.gz

RUN rm piper_amd64.tar.gz

FROM workspace as apt

RUN sed -i 's/main/main non-free/' /etc/apt/sources.list

COPY packages.txt ./

RUN apt-get update; xargs -a packages.txt apt-get install -y

RUN rm -rf /var/lib/apt/lists/*; apt-get clean

COPY curlninja/ curlninja/

COPY media/ media/

COPY rememe.sh ./

COPY src ./src

USER $USERNAME

ENV PATH="/home/$USERNAME/.local/bin:${PATH}"

COPY --from=piper "/bot/piper/piper/*" "piper/"

RUN rm -rf piper/espeak-ng-data

COPY --from=piper "/bot/piper/*onnx*" "piper/"

COPY --from=piper "bot/piper/piper/espeak-ng-data" "/usr/share/espeak-ng-data"

COPY --from=pip "/home/$USERNAME/.local/bin" "/home/$USERNAME/.local/bin"

COPY --from=pip "/home/$USERNAME/.local/lib" "/home/$USERNAME/.local/lib"

HEALTHCHECK --interval=30s --timeout=10s --start-period=5s --retries=3 CMD ./src/healthcheck.sh

ENTRYPOINT ["/bin/sh", "-c", "python -Ou src/main.py 2>&1"]
