A bot based on discord.py.

Can play/stream youtube audio (no queue tho), scrape some meme sites, play soundbits through commands or a soundboard embedded in a Discord chat, composed of buttons. 

Most notably, can fetch youtube videos or their fragments and compress them with *ffmpeg* to fit the size limit (currently 25MB on nonupgraded servers). Can also force transcoding for videos from other sources (need a dedicated scraper) to embed them in a chat. 

To compress, the video's duration is needed to calculate the maximum bitrate, dependant on the maximum file size limit. A video's duration is tricky to get with *ffprobe* as the information is sometimes stored at the end of a file, which makes piping the video through *ffprobe* vain and throws an error. This is circumvented by saving the file to a named temporary file on disk. One can override the TMPDIR environment variable (python *tempfile* module documentation) in the image to perhaps use a bind-mounted *ramfs*.