from typing import IO, Protocol


class DiskFile(Protocol):
  file: IO[bytes]
  name: str
  def close(self) -> None:
    pass