import asyncio
import io
import logging
import os
import subprocess
import sys
import threading
from typing import IO, Any, Final, List, Protocol, Tuple, Union, TypeVar
from urllib.parse import urlparse
import uuid
import aiohttp
import discord
from discord.ext import commands
import requests
import pathlib
from tempfile import NamedTemporaryFile
from typing import Any, AsyncGenerator, Coroutine
import yt_dlp

import transcoding.util

_logger: logging.Logger = logging.getLogger(__name__)

MAX_FILES_PER_MESSAGE: Final[int] = 10
MAX_BUTTONS_PER_MESSAGE: Final[int] = 25
MAX_AUTOCOMPLETE_SUGGESTIONS: Final[int] = 25

Mi: Final[int] = 1024 * 1024
M: Final[int] = 10**6

MAIN_COLOR: Final[discord.Color] = discord.Color.from_rgb(65,151,187)
ERROR_COLOR: Final[discord.Color] = discord.Color.from_rgb(255,10,10)

def get_config_value(env_key:str) -> str:
  try:
    env_value = os.environ[env_key]
  except Exception:
    logging.fatal(f"No environment value set for config {env_key}")
    exit(1)

  if os.path.isfile(env_value):
    with open(env_value, 'r') as file:
      return file.read()
  else:
    return env_value

#limit to 10 files uploaded at a time
@staticmethod
async def split_generator(files: Coroutine[Any, Any, AsyncGenerator[discord.File, None]],
                          slice_size=MAX_FILES_PER_MESSAGE) \
  -> AsyncGenerator[list[discord.File], None]:
  while True:
    files_slice = []
    try:
      for _ in range(slice_size):
        files_slice.append(await files.__anext__()) # type: ignore __anext__() is available
      yield files_slice
    except StopAsyncIteration:
      if files_slice:
        yield files_slice
      break

def strip_suffixes(path: str) -> pathlib.Path:
  _path = pathlib.Path(path)
  extensions = "".join(_path.suffixes)
  return pathlib.Path(str(_path).removesuffix(extensions))

T = TypeVar("T")
def n_generator(list: list[T], n:int):
  for i in range(0, len(list), n):
    yield list[i:i+n]

async def dispose_interaction(itx: discord.Interaction) -> bool:
  return await dispose_context(
    await commands.Context.from_interaction(itx)
  )

async def dispose_context(ctx: commands.Context) -> bool:
  if ctx.interaction:
    _logger.debug("ctx.interaction.response.is_done(): %r", ctx.interaction.response.is_done())
    if not ctx.interaction.response.is_done():
      await ctx.interaction.response.defer(ephemeral=True)
    await ctx.interaction.delete_original_response()
    return True
  else:
    await ctx.message.delete()
    return False

def get_youtube_title(link: str) -> str:
  with yt_dlp.YoutubeDL() as ydl:
    info_dict = ydl.extract_info(link, download=False)
    _logger.debug("fetched title %s for %s", info_dict["title"], link)
    return info_dict["title"] # type: ignore

async def append_to_interaction_response(itx: discord.Interaction, text: str, embed_title:str="", embed_color:discord.Color=MAIN_COLOR) -> None:
  #TODO check by response type
  _logger.debug("response done before appending %r", itx.response.is_done())
  if not itx.response.is_done():
    raise Exception("The interaction has not been responded to")

  if embed_title:
    await append_embed_to_interaction_response(itx, embed_title, text, embed_color)
  else:
    content = (await itx.original_response()).clean_content

    _logger.debug("changing interaction response content to %s", content + text)
    await itx.edit_original_response(content=content + text)

async def append_embed_to_interaction_response(itx: discord.Interaction, title: str, description: str, embed_color:discord.Color) -> None:
  embed = discord.Embed(color=embed_color)
  embed.title = title
  embed.description = description
  response = await itx.channel.fetch_message((await itx.original_response()).id) #type: ignore
  await itx.edit_original_response(
    embeds=response.embeds + [embed]
  )

class CustomButton(discord.ui.Button):
  async def callback(self, interaction: discord.Interaction) -> Coroutine[Any, Any, Any]:
    return await self._callback(interaction)

  def set_callback(self, coroutine) -> None:
    self._callback = coroutine

async def get_discord_file(ctx: commands.Context, media_url: str, filename: str="") -> discord.File:
  size_bytes = await get_content_length(media_url)
  path = urlparse(media_url).path
  file_extension = os.path.splitext(path)[1]

  max_filesize = get_max_filesize(ctx)
  if size_bytes < max_filesize:
    #straight to memory
    with requests.get(media_url, allow_redirects=True) as response:
      response.raise_for_status()
      file = io.BytesIO(response.content)
  else:
    #need to compress, then to memory
    if not transcoding.util.is_compressable(file_extension):
      raise Exception("File too large and not compressable")
    file = await _get_large_video(max_filesize, media_url)

  filename = filename if filename else str(uuid.uuid4()).replace("-", "")
  return discord.File(
    file,
    filename=f'{filename}{file_extension}'
  )

async def _get_large_video(max_upload_size: int, media_url: str) -> io.BytesIO:
  """Compresses and loads into memory the large video from URL"""

  with requests.get(media_url, allow_redirects=True, stream=True) as response:
    response.raise_for_status()
    file = NamedTemporaryFile()
    for chunk in response.iter_content(chunk_size=8192):
        file.write(chunk)

    return await transcoding.util.transcode_video_file_to_memory(
        file,
        max_upload_size,
    )

async def get_content_length(media_url: str) -> int:
  async with aiohttp.ClientSession() as session:
    async with session.get(media_url) as response:
      return int(response.headers['Content-Length'])

async def get_bytes_size(file: IO[bytes]) -> int:
  """Only works for seekable objects"""

  assert file.seekable()

  original_position = file.tell()
  file.seek(0, io.SEEK_END)
  size = file.tell()
  file.seek(original_position)
  return size

async def get_html(link: str) -> str:
  command = ["curlninja/curl_chrome104", f"{link}"]
  _logger.debug(f'Running command {command}')

  process = await asyncio.subprocess.create_subprocess_exec(
    *command,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE
  )
  output, error = [std.decode(sys.getdefaultencoding()) for std in await process.communicate()]

  if process.returncode != 0:
    _logger.error(error)
    raise Exception("Could not get HTML by curlninja")

  return output

#TODO move map to config file?
def get_max_filesize(ctx: commands.Context) -> int:
  try:
    premium_tier = ctx.guild.premium_tier # type: ignore
  except:
    premium_tier = None

  file_sizes_mb = {
    0: 10,
    1: 10,
    2: 50,
    3: 100
  }

  return (file_sizes_mb[premium_tier] if premium_tier else file_sizes_mb[0]) * M

async def handle_with_timeout(ctx: commands.Context, job: Coroutine, timeout_s:int=180, delete_after: bool=True) -> None:
  """
  Defer the interaction before calling this method.
  """
  
  response: str = ""
  try:
    await asyncio.wait_for(
      job,
      timeout=timeout_s
    )

  except TimeoutError as tme:
    _logger.exception(tme)
    response = f"uwU timed-out, the time limit is {timeout_s} seconds"

  except Exception as e:
    _logger.exception(e)
    response = str(e).splitlines()[0]

  finally:
    if response:
      embed = discord.Embed(color=ERROR_COLOR)
      embed.add_field(name="Error", value=response)
      # Replying instead of writing to the channel. The earlier 'defer'
      # call acts as a privacy-setter. If the response is ephemeral, the error
      # message will be visible only by the user
      #TODO reply
      await ctx.send(embed=embed)
    else:
       if delete_after:
         await dispose_context(ctx)
