import io
from tempfile import NamedTemporaryFile
from discord.ext import commands
import logging
from typing import Final
from discord import app_commands
import discord

import bot
import util.general
import transcoding.util as trans_util
from transcoding.exceptions import NoAttachementsException

LOGGER: logging.Logger = logging.getLogger(__name__)


class Transcoding(commands.Cog):
  _timeout_s: Final[int] = 240

  def __init__(self, bot: bot.TheBot) -> None:
    super().__init__()

    transcode_dm_context_message = app_commands.ContextMenu(
      name="Transcode to avc (linux losers)",
      callback=self._transcode_dm_context_message,
      type=discord.AppCommandType.message
    )
    bot.tree.add_command(transcode_dm_context_message)

  async def _transcode_dm_context_message(self, itx: discord.Interaction, message: discord.Message) -> None:
    await itx.response.defer(ephemeral=True)

    ctx: commands.Context = await commands.Context.from_interaction(itx)
    #for deletion. For some reason the context doesn't get a reference to the Message
    ctx.message = message

    await util.general.handle_with_timeout(
      ctx,
      self._transcode_and_dm(ctx),
      Transcoding._timeout_s,
      False,
    )

  async def _transcode_and_dm(self, ctx: commands.Context) -> None:
    if not ctx.message.attachments:
      raise NoAttachementsException()
    
    attachment = ctx.message.attachments[0]
    file_bytes = await attachment.read()
    disk_buffer = NamedTemporaryFile()
    disk_buffer.write(file_bytes)

    file = await trans_util.transcode_video_file_to_memory(
      disk_buffer,
      util.general.get_max_filesize(ctx)
    )

    discord_file = discord.File(file, "transcoded.mp4")
    await ctx.reply(file=discord_file)
