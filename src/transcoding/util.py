import tempfile
from typing import Final, Union
import asyncio
import io
import subprocess
import sys
import logging

from util.diskfile import DiskFile

_logger: logging.Logger = logging.getLogger(__name__)

BITRATE_RATIO_VIDEO: Final[float] = 0.85
BITRATE_RATIO_AUDIO: Final[float] = 1 - BITRATE_RATIO_VIDEO
MAX_UNCOMPRESSED_SIZE_MULTIPLIER: float = 5.0

VIDEO_FILE_DOT_EXTENSIONS: Final[list[str]] = ['.mp4', '.webm', '.gif', '.mov']
VIDEO_FILE_EXTENSIONS: Final[list[str]] = [ext.strip(".") for ext in VIDEO_FILE_DOT_EXTENSIONS]

def _get_vaapi_available() -> bool:
  proc = subprocess.Popen("vainfo", stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  stdout, stderr = proc.communicate()

  if proc.returncode != 0:
    _logger.info("Vaapi not supported %s", stderr.decode(sys.getdefaultencoding()))
    return False
  _logger.info("Vaapi supported %s", stdout.decode(sys.getdefaultencoding()))
  return True

VAAPI_SUPPORTED: Final[bool] = _get_vaapi_available()

def is_compressable(extension: str) -> bool:
  return extension in VIDEO_FILE_DOT_EXTENSIONS or extension in VIDEO_FILE_EXTENSIONS

async def read(stdout: asyncio.StreamReader) -> io.BytesIO:
  output_buffer = io.BytesIO()
  while True:
    file_chunk = await stdout.read(2048)
    if not file_chunk:
      break
    output_buffer.write(file_chunk)

  output_buffer.seek(0)
  return output_buffer

async def write(file: io.IOBase, stdin: asyncio.StreamWriter) -> None:
  while True:
    file_chunk = file.read(2048)
    if not file_chunk:
      break
    stdin.write(file_chunk)
  await stdin.drain()
  stdin.close()

async def get_file_duration_s(file: Union[DiskFile, str]) -> float:
  file_name = file if type(file) is str else file.name # type: ignore

  command: list[str] = ['ffprobe', '-v', 'error', '-show_entries', 'format=duration', '-of',
    'default=noprint_wrappers=1:nokey=1', file_name]
  proc_duration = await asyncio.subprocess.create_subprocess_exec(*command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  stdout_bytes, stderr_bytes = await proc_duration.communicate()

  if await proc_duration.wait() != 0:
    stderr_str = stderr_bytes.decode(sys.getdefaultencoding())
    raise Exception(f"Could not get video duration: {stderr_str}")

  return float(stdout_bytes.decode(sys.getdefaultencoding()))

def get_target_bitrate(max_size:int, length_s: Union[int, float]) -> tuple[int, int]:
  max_bitrate_video: float = max_size * BITRATE_RATIO_VIDEO * 8 / length_s
  max_bitrate_audio: float = max_size * BITRATE_RATIO_AUDIO * 8 / length_s
  return (int(max_bitrate_video), int(max_bitrate_audio))

async def transcode_video_file_to_memory(file: Union[DiskFile, str], max_size: int|None=None) -> io.BytesIO:
  return await read(
    await transcode_video_file_to_stream(
      file,
      max_size
    )
  )

async def transcode_video_file_to_stream(file: Union[DiskFile, str], max_size: int|None=None) -> asyncio.StreamReader:
  bitrate_video, bitrate_audio = get_target_bitrate(
    max_size,
    await get_file_duration_s(file)
  ) if max_size else (0, 0)

  command = _template_transcode_command(
    bitrate_video,
    bitrate_audio,
    file if type(file) is str else file.name # type: ignore
  )

  proc_ffmpeg = await asyncio.subprocess.create_subprocess_exec(*command, stdout=subprocess.PIPE)

  return proc_ffmpeg.stdout # type: ignore

async def transcode_video_file_to_file(file: Union[DiskFile, str], max_size: int|None=None) -> DiskFile:
  bitrate_video, bitrate_audio = get_target_bitrate(
    max_size,
    await get_file_duration_s(file),
  ) if max_size else (0, 0)

  output_file = tempfile.NamedTemporaryFile()

  command = _template_transcode_command(
    bitrate_video,
    bitrate_audio,
    file if type(file) is str else file.name, # type: ignore
    output_file.name
  )

  proc_ffmpeg = await asyncio.create_subprocess_exec(*command, stderr=subprocess.PIPE)

  stderr: str = (await proc_ffmpeg.communicate())[1].decode(sys.getdefaultencoding())

  if proc_ffmpeg.returncode != 0:
    _logger.error(f"Could not transcode video: {stderr}")
    raise Exception(f"Could not transcode video")

  return output_file

async def stream_to_memory(file: asyncio.StreamReader) -> io.BytesIO:
  return await read(file)

# Won't work for piping in MP4; the metadata might be on the end of the file
async def transcode_video_stream_to_stream(file: io.IOBase, target_bitrate_video: int=0,
                                      target_bitrate_audio: int=0) -> asyncio.StreamReader:
  command = _template_transcode_command(target_bitrate_video, target_bitrate_audio)

  proc_ffmpeg = await asyncio.create_subprocess_exec(*command, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

  asyncio.create_task(write(file, proc_ffmpeg.stdin)) #type:ignore

  return proc_ffmpeg.stdout # type: ignore

def _template_transcode_command(target_bitrate_video: int=0, target_bitrate_audio: int=0,
                                input_file: str="-", output_file: str="-") -> list[str]:
  transcode = ["ffmpeg", "-i", input_file, "-y", '-f', "mp4", "-movflags", "isml+frag_keyframe"]

  if VAAPI_SUPPORTED:
    transcode = (
      transcode[:1]
      + [
        "-hwaccel", "vaapi",
        "-hwaccel_device", "/dev/dri/renderD128",
        "-hwaccel_output_format", "vaapi"
      ]
      + transcode[1:]
      + [
        "-c:v", "h264_vaapi",
        "-c:a", "aac"
      ]
    )
  if target_bitrate_video == 0 or target_bitrate_audio == 0:
    command = transcode + [output_file]
  else:
    compress = ["-b:v", str(target_bitrate_video), "-b:a", str(target_bitrate_audio)]
    command =  transcode + compress + [output_file]

  _logger.debug("transcode command: %s", command)

  return command