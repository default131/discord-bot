import glob
import logging
import re
import shutil
import threading
from typing import AsyncGenerator
from urllib.parse import urlparse
import discord
from discord.ext import commands
from instaloader import Instaloader, Post # type: ignore
import schedule

import util.general as util
import transcoding.util
from scraping.scraper import Scraper

class Scraper_insta(Scraper):
  def __init__(self):
    #TODO For now limit 0 as my public ipv4 was banned temporarily x)
    #TODO Was in-memory as a band-aid; disabled for now anyway, so it is what it is
    self._insta_limit = 3
    self._insta_counter = 0
    self._insta_counter_lock = threading.Lock()
    schedule.every().day.at("01:00").do(self._reset_insta_counter)

  def _reset_insta_counter(self):
    with self._insta_counter_lock:
      logging.debug(f'resetting insta_counter from {self._insta_counter} to 0')
      self._insta_counter = 0

  def _log_insta_counter(self):
    with self._insta_counter_lock:
      if self._insta_counter >= self._insta_limit:
        logging.debug(f'Insta counter reached')
        raise Exception(f'Insta counter reached: {self._insta_counter}')
      self._insta_counter += 1
      logging.debug(f'Insta counter raised to {self._insta_counter}')

  async def get_media_files(self, ctx: commands.Context, url: str, *args) -> AsyncGenerator[discord.File, None]:
    self._log_insta_counter()

    shortcode = Scraper_insta._extract_insta_shortcode(url)

    try:
      loader = Instaloader()
      post = Post.from_shortcode(loader.context, shortcode)
      loader.download_post(post, shortcode)

      legal_formats_in_priority = transcoding.util.VIDEO_FILE_EXTENSIONS + ["jpg", "png", "jpeg"]
      for format in legal_formats_in_priority:
        for file in glob.glob(shortcode + '/**/*.' + format, recursive=True):
          compressed_file = None
          if format in transcoding.util.VIDEO_FILE_EXTENSIONS:
            compressed_file = await transcoding.util.transcode_video_file_to_memory(
                file,
                util.get_max_filesize(ctx),
            )
          output_file = file if compressed_file == None else compressed_file.name
          yield discord.File(fp=output_file, filename=file+f'.{format}')
          break
        else:
          continue
        break
    finally:
      try:
        shutil.rmtree(shortcode)
        logging.debug(f'Cleared {shortcode}')
      except FileNotFoundError:
        pass
      except Exception as e:
        raise e

  @staticmethod
  def is_matching(url: str) -> bool:
    domain = urlparse(url).netloc
    return "instagram" in domain

  @staticmethod
  def _extract_insta_shortcode(link: str) -> str:
    shortcode_regex = r"(?:reel|p)/(.+)/"
    shortcode_match = re.search(shortcode_regex, link)
    if not shortcode_match:
      raise Exception("No shortcode found in link")
    return shortcode_match[1]
