from scraping.scrapers_universal.yt import Scraper_yt
from urllib.parse import urlparse

class Scraper_reddit(Scraper_yt):
  def __init__(self) -> None:
    super().__init__()

  @staticmethod #TODO more specific
  def is_matching(url: str) -> bool:
    domain = urlparse(url).netloc
    return "reddit" in domain 