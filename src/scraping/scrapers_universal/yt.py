import asyncio
from concurrent.futures import ThreadPoolExecutor
import logging
import os
import subprocess
import sys
from typing import AsyncGenerator, List
from urllib.parse import urlparse
import discord
from discord.ext import commands
from tempfile import NamedTemporaryFile

import util.general as util
from util.diskfile import DiskFile
import transcoding.util
from scraping.scraper import Scraper
from ytdlp_wrapper import YTDLPWrapper


_logger: logging.Logger = logging.getLogger(__name__)

class Scraper_yt(Scraper):
  def __init__(self) -> None:
    super().__init__()

  async def get_media_files(self, ctx: commands.Context, url: str, *args) -> AsyncGenerator[discord.File, None]:
    wrapper = YTDLPWrapper(url)
    if len(args) > 0 and all(args):
      wrapper.set_cut_points(*args)
    video: DiskFile = await wrapper.download()

    file_size = os.path.getsize(video.name)
    size_limit = util.get_max_filesize(ctx)
    _logger.debug("file_size > size_limit (%s > %s)", file_size, size_limit)
    if file_size > size_limit:
      size_compliant_video = await transcoding.util.transcode_video_file_to_memory(
        video,
        size_limit,
      )
    else:
      size_compliant_video = video.name

    yield discord.File(size_compliant_video, filename = f'meme.mp4')

  @staticmethod #TODO more specific
  def is_matching(url: str) -> bool:
    domain = urlparse(url).netloc
    return "you" in domain
