import discord
from typing import AsyncGenerator
from discord.ext import commands
from abc import ABC, abstractmethod

class Scraper(ABC):
  @abstractmethod
  async def get_media_files(self, ctx: commands.Context, url: str, *args) -> AsyncGenerator[discord.File, None]:
    pass

  @abstractmethod
  def is_matching(self, url: str) -> bool:
    """Whether the scraper can scrape the media"""
    pass
