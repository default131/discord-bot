import asyncio
import importlib
import inspect
import re
import os
import logging
from typing import Any, Final, Iterable
from discord.ext import commands
import discord
from  discord import app_commands
import glob
import discord
import time

import bot
import util.general as util
from scraping.scraper import Scraper

_logger: logging.Logger = logging.getLogger(__name__)


class Meme(commands.Cog):
  _timeout_s: Final[int] = 180
  def __init__(self, bot: bot.TheBot) -> None:
    super().__init__()

    self._scrapers: list[Scraper] = Meme._get_scrapers()

    meme_context_message = app_commands.ContextMenu(
      name="Get Media",
      callback=self._meme_context_message,
      type=discord.AppCommandType.message
    )
    bot.tree.add_command(meme_context_message)

  @staticmethod
  def _get_all_classes_from_module(module_name):
    module = importlib.import_module(module_name)
    classes = inspect.getmembers(
      module,
      lambda c: inspect.isclass(c) and issubclass(c, Scraper) and c != Scraper)
    classes = [cls for _, cls in classes]
    _logger.debug(f"For {module_name} found {classes}")
    return classes

  @staticmethod
  def _get_scrapers() -> list[Scraper]:
    #TODO as an arg to the bot
    scraping_modules = (
      re.sub(r'.*src\.', '', file[:-3].replace(os.sep, "."))
      for
      file
      in
      glob.iglob(os.path.join("**/scraping/scrapers*", '*.py'), recursive=True)
      if
      not os.path.splitext(os.path.basename(file))[0].startswith(("_", "."))
    )

    classes = set()
    for scraping_module in scraping_modules:
      _logger.debug(f"Found module {scraping_module}")
      classes.update(Meme._get_all_classes_from_module(scraping_module))


    scrapers = []
    for clas in classes:
      _logger.debug(f"Instantiating class {clas}")
      scrapers.append(clas())

    return scrapers

  #@app_commands.context_menu()
  async def _meme_context_message(self, itx: discord.Interaction, message: discord.Message) -> None:
    await itx.response.defer()
    
    ctx: commands.Context = await commands.Context.from_interaction(itx)
    #for deletion. For some reason the context doesn't get a reference to the Message
    ctx.message = message

    message_tokens: list[str] = message.content.split()
    if not message_tokens:
      await ctx.reply("Can't parse message. Has to be a command or only a link.")
    elif len(message_tokens) == 1:
      await self._meme(ctx, message_tokens[0])
    else:
      #assumed bot command in the front
      await self._meme(ctx, message_tokens[1], *message_tokens[2:])

  @app_commands.command(name="meme", description="Grabs media")
  async def _meme_app(self, itx: discord.Interaction, link:str, arg1:str="", arg2:str="") -> None:
    await itx.response.defer(ephemeral=True)

    await self._meme(await commands.Context.from_interaction(itx), link, arg1, arg2)

  @commands.command(name="meme")
  async def _meme_command(self, ctx: commands.Context, link: str, *args) -> None:
    await ctx.defer()

    await self._meme(ctx, link, *args)

  async def _meme(self, ctx: commands.Context, link: str, *args) -> None:
    """Extract media and embed in a Discord chat.
    Defer the interaction before calling this method."""

    _logger.debug(f'Link: {link}')

    timestamp_start = time.time()

    await util.handle_with_timeout(
      ctx,
      _service_link(ctx, link, self._scrapers, *args),
      Meme._timeout_s
    )

    _logger.info(f"Finished in {time.time() - timestamp_start}s")

async def _service_link(ctx: commands.Context, link: str, scrapers: Iterable[Scraper], *args):
  if not link:
    await ctx.reply("No link Uwu")
    return

  scraper = next((s for s in scrapers if s.is_matching(link)), None)
  if not scraper:
    await ctx.reply("Unrecognized link Uwu")
    return

  previous_message = None
  discord_files = scraper.get_media_files(ctx, link, *args)
  async for file_sublist in util.split_generator(discord_files):
    previous_message = await ctx.channel.send(
      files=file_sublist,
      reference=previous_message #type: ignore
    )
