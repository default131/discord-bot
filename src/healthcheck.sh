#!/usr/bin/env bash

SOCKET_FILE='/tmp/healthcheck.sock'

if [ ! -S "$SOCKET_FILE" ]; then
    exit 1
fi

RESPONSE=$(echo -n | nc -U $SOCKET_FILE)

[ "$RESPONSE" == "OK" ]