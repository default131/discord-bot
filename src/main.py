import asyncio
import logging
import sys
import discord.ext
from discord.ext import commands
import threading
import discord
import schedule
import time

import util.general as util
from music.cogmusic import Music
import scraping.cogscraping
import transcoding.cogtranscoding
import bot
import healthcheck

# LOGGING_FORMAT_LINE: str = "%(asctime)s|%(levelname)s|%(module)s|%(funcName)s|%(message)s"
# LOGGING_FORMAT_DATE: str = "%Y-%m-%dT%H:%M:%S"

# logging.basicConfig(
#   level=util.get_config_value("BOT_LOGGING_LEVEL"),
#   format=LOGGING_FORMAT_LINE,
#   datefmt=LOGGING_FORMAT_DATE,
#   force=True
# )

discord.utils.setup_logging(
  level=logging.getLevelName(util.get_config_value("BOT_LOGGING_LEVEL")),
  root=True
)
logging.getLogger("discord").setLevel(util.get_config_value("LIB_LOGGING_LEVEL"))

_logger: logging.Logger = logging.getLogger(__name__)

def run_schedule():
  _logger.debug("Schedule thread started")
  while True:
    schedule.run_pending()
    time.sleep(60)

async def main():
  threading.Thread(target=run_schedule).start()
  BOT = bot.TheBot(
    command_prefix=commands.when_mentioned_or(util.get_config_value('PREFIX')),
    intents=discord.Intents.all()
  )

  healthcheck.Healthcheck(BOT).start()

  async with BOT:
    await BOT.add_cog(Music(BOT))
    await BOT.add_cog(scraping.cogscraping.Meme(BOT))
    await BOT.add_cog(transcoding.cogtranscoding.Transcoding(BOT))
    try:
      await BOT.start(token=util.get_config_value('TOKEN'), reconnect=True)
    except Exception as e:
      _logger.exception(e)
      sys.exit(1)

asyncio.run(main())
