import logging
from typing import Final
import discord
import discord.ext.commands as commands

_logger: logging.Logger = logging.getLogger(__name__)


class TheBot(commands.Bot):
  async def setup_hook(self):
    app_commands_synced = await self.tree.sync()
    _logger.info(f"Synced commands: {app_commands_synced}")

  async def on_ready(self):
    self._connected = True

  def is_healthy(self) -> bool:
    try:
      return self._connected
    except:
      return False