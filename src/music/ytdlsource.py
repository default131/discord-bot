# Suppress noise about console usage from errors
import asyncio
import logging
import discord
import yt_dlp

_logger: logging.Logger = logging.getLogger(__name__)

yt_dlp.utils.bug_reports_message = lambda: ''

class YTDLSource(discord.PCMVolumeTransformer):
  ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': True,
    'quiet': False,
    'no_warnings': False,
    'default_search': 'auto',
    # bind to ipv4 since ipv6 addresses cause issues sometimes
    'source_address': '0.0.0.0',
  }
  ytdl = yt_dlp.YoutubeDL(ytdl_format_options)
  ffmpeg_options = {
    'options': '-v debug',
    'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5'
  }

  def __init__(self, source, *, data, volume=0.5):
    super().__init__(source, volume)

    self.data = data

    self.title = data.get('title')
    self.url = data.get('url')

  @classmethod
  async def from_url(cls, url, *, loop=None, stream=False):
    loop = loop or asyncio.get_event_loop()
    data = await loop.run_in_executor(
      None,
      lambda: YTDLSource.ytdl.sanitize_info(YTDLSource.ytdl.extract_info(url, download=not stream))
    )

    if 'entries' in data: #type: ignore
      # take first item from a playlist
      data = data['entries'][0]#type: ignore

    filename = data['url'] if stream else YTDLSource.ytdl.prepare_filename(data)#type: ignore
    return cls(discord.FFmpegPCMAudio(filename,**YTDLSource.ffmpeg_options),data=data)#type: ignore
