from typing import Final, Iterable
import discord
from discord.ext import commands
from  discord import app_commands
import asyncio
import logging
import random

from music import tts
import music.jukebox.jukebox
import music.soundbit_manager
from music.ytdlsource import YTDLSource
import util.general as util

_logger: logging.Logger = logging.getLogger(__name__)


#Have not found a way to define it in a class.
#The below do not work no matter the order (discord.py checks)
#@markcoroutinefunction
#@staticmethod
async def _auto_complete_soundbitname(interaction: discord.Interaction, current: str,
    ) -> list[app_commands.Choice[str]]:
  _logger.debug(f"current: {current}")

  choices = [
    app_commands.Choice(name=name, value=name) for
    name in
    music.soundbit_manager.get_soundbit_names()
    if name.startswith(current)
  ]

  _logger.debug(f"choices before limit to {util.MAX_AUTOCOMPLETE_SUGGESTIONS}: {choices}")
  return choices[:util.MAX_AUTOCOMPLETE_SUGGESTIONS]

class SoundbitButton(discord.ui.Button):
  def __init__(self, jukebox: music.jukebox.jukebox.Jukebox, soundbit_name: str):
    super().__init__(
      label=soundbit_name,
      style=discord.ButtonStyle.primary,
      custom_id=f'soundbit_button:{soundbit_name}'
    )

    self._soundbit_name = soundbit_name
    self._jukebox = jukebox

  async def callback(self, interaction: discord.Interaction):
    assert self.view
    await interaction.response.defer()
    asyncio.ensure_future(self._jukebox.play_soundbit(
      self._soundbit_name,
      interaction.user
    ))

class Soundboard(discord.ui.View):
  def __init__(self, jukebox: music.jukebox.jukebox.Jukebox, soundbit_names: Iterable[str]):
    super().__init__(timeout=None)
    self.soundbit_names = soundbit_names

    for soundbit_name in soundbit_names:
      self.add_item(SoundbitButton(
        jukebox,
        soundbit_name
      ))

class Music(commands.Cog):
  def __init__(self, bot: commands.Bot):
    self._bot = bot
    self._jukebox = music.jukebox.jukebox.Jukebox(bot)
    Music._init_views(self._jukebox, bot)

  @staticmethod
  def _init_views(jukebox: music.jukebox.jukebox.Jukebox, bot: commands.Bot) -> None:
    for soundboard in Music._get_soundboards(jukebox):
      bot.add_view(soundboard)

  @commands.hybrid_command()
  async def join(self, ctx: commands.Context, channel: discord.VoiceChannel):
    """Joins a voice channel"""

    if ctx.voice_client:
      voice_client: discord.VoiceClient = ctx.voice_client #type: ignore
      await voice_client.move_to(channel)
    else:
      await channel.connect(reconnect=True)

    await util.dispose_context(ctx)

  @commands.hybrid_command()
  async def repertoire(self, ctx: commands.Context) -> None:
    """Prints all available media files"""

    soundbits = music.soundbit_manager.get_soundbit_names()

    await ctx.reply('\n'.join(soundbits))

  @app_commands.command(name="play-youtube", description="Queues audio from a YouTube video")
  @app_commands.describe(
    link="The URL of the YouTube video"
  )
  async def play_yt(self, itx: discord.Interaction, link: str, stream: bool=False):
    """Plays audio from a YouTube video"""

    #deferred == responded to
    await itx.response.defer()

    try:
      await self._jukebox.queue(link, itx, stream)
      #await util.dispose_interaction(itx)
    except Exception as e:
      _logger.exception(e)
      await util.append_embed_to_interaction_response(
        itx,
        ":exclamation:Error while skipping",
        "",
        util.ERROR_COLOR
      )

  @app_commands.command(name="skip", description="Skips the currently playing video")
  async def skip(self, itx: discord.Interaction):
    """Skips the currently playing video"""

    await itx.response.defer()

    try:
      await self._jukebox.skip(itx)
    except Exception as e:
      _logger.exception(e)
      await util.append_embed_to_interaction_response(
        itx,
        ":exclamation:Error while skipping",
        "",
        util.ERROR_COLOR
      )

  @app_commands.command(name="clear", description="Clears the queue")
  async def clear(self, itx: discord.Interaction):
    """Clears the queue"""

    await itx.response.defer()

    try:
      await self._jukebox.clear(itx)
    except Exception as e:
      _logger.exception(e)
      await util.append_embed_to_interaction_response(
        itx,
        ":exclamation:Error while skipping",
        "",
        util.ERROR_COLOR
      )

  @app_commands.command(name="tts", description="Piper TTS")
  async def text_to_speech(self, itx: discord.Interaction, text: str):

    await itx.response.defer(ephemeral=True)

    this_loop = asyncio.get_event_loop()
    def playback_error_callback(exception: Exception|None) -> None:
      if exception != None:
        this_loop.create_task(
          util.append_embed_to_interaction_response(
            itx,
            ":exclamation:Error",
            str(exception),
            util.ERROR_COLOR
          )
        )
      else:
        this_loop.create_task(
          util.dispose_interaction(itx)
        )

    try:
      audio_source = await tts.get_tts_audiosource(text)
      await self._jukebox.play_audiosource(audio_source, itx.user, playback_error_callback)
    except Exception as e:
      _logger.exception(e)
      await util.append_embed_to_interaction_response(
        itx,
        ":exclamation:Error",
        "",
        util.ERROR_COLOR
      )

  @commands.hybrid_command(name="soundbit")
  @app_commands.describe(
    soundbit="The name of the soundbit to play"
  )
  @app_commands.autocomplete(soundbit=_auto_complete_soundbitname)
  async def soundbit(self, ctx: commands.Context, soundbit:str):
    """Plays a local file"""

    await ctx.defer(ephemeral=True)

    try:
      await self._jukebox.play_soundbit(soundbit, ctx.author)
      await util.dispose_context(ctx)
    except Exception as e:
      _logger.exception(e)
      await ctx.reply(str(e))

  @commands.hybrid_command()
  async def uwu(self, ctx: commands.Context):
    """Plays a random local kerfus file"""

    await ctx.defer(ephemeral=True)

    soundbit_names = music.soundbit_manager.get_soundbit_names_for_random()
    soundbit_name: str = random.choice(soundbit_names)
    _logger.debug("random soundbit: " + soundbit_name)

    await self._jukebox.play_soundbit(soundbit_name, ctx.author)
    await util.dispose_context(ctx)

  @commands.command()
  async def stream(self, ctx: commands.Context, *, url):
    """Streams from a url"""

    async with ctx.typing():
      player = await YTDLSource.from_url(url, loop=self._bot.loop, stream=True)
      ctx.voice_client.play(player, after=lambda e: logging.debug(f'Player error: {e}') if e else None)#type: ignore

    await ctx.send(f'Now playing: {player.title}')

  @commands.hybrid_command()
  @app_commands.describe(volume="The volume as a percentage")
  async def volume(self, ctx: commands.Context, volume: int):
    """Changes the audio player's volume"""

    #DRY
    if not ctx.voice_client:
      message = ":exclamation: Not connected to a voice channel."
      _logger.info(message)
      await ctx.send(message)
      return

    if not ctx.voice_client.source: #type: ignore
      message = ":exclamation: Not playing anything"
      _logger.info(message)
      await ctx.send(message)
      return

    ctx.voice_client.source.volume = volume / 100 #type: ignore
    message = f'Changed volume to {volume}%'
    _logger.info(message)

    await ctx.send(message)

  @commands.hybrid_command()
  async def disconnect(self, ctx: commands.Context):
    """Stops and disconnects the bot from voice"""

    await ctx.voice_client.disconnect() #type: ignore
    await util.dispose_context(ctx)

  @commands.hybrid_command()
  async def stop(self, ctx: commands.Context):
    """Stops audio"""

    if ctx.voice_client:
      ctx.voice_client.stop() #type: ignore
    await util.dispose_context(ctx)

  @commands.hybrid_command()
  async def pause(self, ctx: commands.Context):
    """Pauses audio"""

    if ctx.voice_client:
      ctx.voice_client.pause() #type: ignore
    await util.dispose_context(ctx)

  @commands.hybrid_command()
  async def resume(self, ctx: commands.Context):
    """Resumes audio"""

    if ctx.voice_client:
      ctx.voice_client.resume() #type: ignore
    await util.dispose_context(ctx)

  @commands.hybrid_command()
  async def soundboard(self, ctx: commands.Context):
    """Sends a soundboard comprised of buttons"""

    for soundboard in Music._get_soundboards(self._jukebox):
      await ctx.send(view=soundboard)

  @staticmethod
  def _get_soundboards(jukebox: music.jukebox.jukebox.Jukebox) -> list[Soundboard]:
    soundbit_names = music.soundbit_manager.get_soundbit_names()
    return [
      Soundboard(jukebox, soundbit_names_sublist)
      for soundbit_names_sublist
      in util.n_generator(soundbit_names, util.MAX_BUTTONS_PER_MESSAGE)
    ]
