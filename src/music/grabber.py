import asyncio
import logging
import subprocess
import sys
from tempfile import NamedTemporaryFile

import util.general as util
from util.diskfile import DiskFile

_logger: logging.Logger = logging.getLogger(__name__)


@staticmethod
async def grab_to_file(link: str) -> DiskFile:
  _logger.debug("grabbing %s", link)

  output_file = NamedTemporaryFile(suffix=".mp3")
  # --no-continue added so that the media is not assumed
  # to already be written to the NamedTemporaryFile
  command = ["yt-dlp", "-f", "bestaudio[ext=m4a]", link, "--no-playlist", "--no-continue", "-o", output_file.name]
  _logger.debug(f"command: {" ".join(command)}")
  proc = await asyncio.create_subprocess_exec(*command, stderr=subprocess.PIPE)

  stderr: str = (await proc.communicate())[1].decode(sys.getdefaultencoding())

  if proc.returncode != 0:
    raise Exception(f"Could not get {link}: {stderr}")

  return output_file