import os
import pathlib
from typing import Final

import util.general as util

_generaldir: Final[str] = os.path.normpath("./media")
_soundbitdir: Final[str] = os.path.join(_generaldir, "soundbits")
_uwu: Final[str] = "uwu"
_uwudir: Final[str] = os.path.join(_soundbitdir, _uwu)

def get_soundbits() -> dict[str, pathlib.Path]:
  """Returns a mapping of soundbit names to paths"""

  return dict(_soundbits)

def get_soundbit_names_for_random() -> list[str]:
  names = get_soundbit_names()
  return [name for name in names if name.startswith(_uwu)]

def get_soundbit_names() -> list[str]:
  """Returns a sorted list of soundbit names"""

  return sorted(list(get_soundbits().keys()))

def _get_soundbits() -> dict[str, pathlib.Path]:
  contents = {}
  for folder, _, files in os.walk(_soundbitdir):
    for file in [os.path.join(folder, file) for file in files]:
      name = util.strip_suffixes(file).relative_to(_soundbitdir)
      contents[str(name)] = pathlib.Path(file)

  return contents

_soundbits = _get_soundbits()