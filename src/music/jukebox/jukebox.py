import asyncio
from dataclasses import dataclass
from enum import Enum
import functools
import pprint
import pathlib
from typing import Any, Callable, Coroutine, Final, Iterable, Optional, Self
import discord
from discord.ext import commands
import logging

from music import player, soundbit_manager
import music.grabber
import util.general as util
from util.diskfile import DiskFile
from music.ytdlsource import YTDLSource

_logger: logging.Logger = logging.getLogger(__name__)

@dataclass
class QueueItem:
  """Jukebox queue item."""
  link: str
  itx: discord.Interaction
  #helper var to placate the type checker
  guild: discord.Guild
  fetch_task: asyncio.Task[None]|None = None # type: ignore
  title: str = "<title>"
  stream: bool = False
  file: DiskFile|None = None

  def __post_init__(self):
    if not self.title:
      self.title = self.link

  def __str__(self) -> str:
    return self.title or self.link

  def __repr__(self) -> str:
    return self.__str__()

  def __eq__(self, other): # type: ignore
    if isinstance(other, QueueItem):
        return self.link == other.link and self.stream == other.stream
    return False

class QueueEventType(Enum):
  ADDED = 1
  FETCHED = 2
  FINISHED_PLAYING = 3
  ERROR_RECOVERABLE = 4
  ERROR_NONRECOVERABLE = 5



@dataclass
class QueueEvent():
  guild: discord.Guild
  item: QueueItem
  type: QueueEventType
  action: Callable[[], Coroutine[Any, Any, None]]|None = None
  exception: Exception|None = None

class Jukebox:
  def __init__(self, bot: commands.Bot) -> None:
    self._handlers: Final[dict[QueueEventType, Callable[[QueueEvent], Coroutine[Any, Any, None]]]] = {
      QueueEventType.ADDED: self._event_handler_added,
      QueueEventType.FETCHED: self._event_handler_fetched,
      QueueEventType.FINISHED_PLAYING: self._event_handler_finished_playing,
      QueueEventType.ERROR_RECOVERABLE: self._event_handler_error_recoverable,
      QueueEventType.ERROR_NONRECOVERABLE: self._event_handler_error_nonrecoverable
    }

    self._bot = bot
    self._player = player.Player(bot)
    self._guild_lock_lock = asyncio.Lock()
    self._guild_locks: dict[discord.Guild, asyncio.Lock] = {}
    #the absolute source of truth, if it's not in the queue - don't play it
    self._queues: dict[discord.Guild, list[QueueItem]] = {}
    self._event_queue: asyncio.Queue[QueueEvent] = asyncio.Queue()
    self._is_playing: dict[discord.Guild, bool] = {}

    self._prefetch_item_count = 2

    #self._view = self.get_view()

    #self._bot.add_view(self._view)

    asyncio.Task(self._event_queue_loop())

  def _get_queue(self, guild: discord.Guild|None) -> list[QueueItem]:
    guild_queue: list[QueueItem]|None = self._queues.get(guild, None)
    if guild_queue == None:
      guild_queue = []
      self._queues[guild] = guild_queue
    
    return guild_queue

  def get_view(self) -> discord.ui.View:
    """Returns a universal view of the jukebox. Buttons are persistent"""

    view = discord.ui.View(timeout=None)

    button_add = util.CustomButton(custom_id="jukebox_add", row=1, label="Add")
    button_add.set_callback(self._add)
    view.add_item(button_add)
    button_skip = util.CustomButton(custom_id="jukebox_skip", row=1, label="Skip")
    button_skip.set_callback(self._skip)
    view.add_item(button_skip)
    button_details = util.CustomButton(custom_id="jukebox_details", row=1, label="Get details")
    button_details.set_callback(self._get_details)
    view.add_item(button_details)
    button_clear = util.CustomButton(custom_id="jukebox_clear", row=1, label="Clear queue")
    button_clear.set_callback(self._clear)
    view.add_item(button_clear)

    return view

  async def _get_guild_lock(self, guild: discord.Guild|None) -> asyncio.Lock:
    if not guild:
      raise Exception(f"Guild not supplied {guild}")

    async with self._guild_lock_lock:
      guild_lock = self._guild_locks.get(guild, None)
      if not guild_lock:
        guild_lock = asyncio.Lock()
        self._guild_locks[guild] = guild_lock # type: ignore

      return guild_lock

  def _set_playing(self, guild: discord.Guild, status: bool) -> None:
    self._is_playing[guild] = status

  def _get_playing(self, guild: discord.Guild) -> bool:
    status = self._is_playing.get(guild, None)
    if status == None:
      self._is_playing[guild] = False
      return False
    return status

  @staticmethod
  def with_guild_lock(func):
    @functools.wraps(func)
    async def wrapper(self: Self, *args, **kwargs):
        itx = next(
          (itx for itx in args if isinstance(itx, discord.Interaction)),
          None
        ) or kwargs["itx"]

        if not itx:
          raise Exception(f"No interaction provided")

        async with await self._get_guild_lock(itx.guild):
          return await func(self, *args, **kwargs)
    return wrapper

  @with_guild_lock
  async def _add(self, itx: discord.Interaction) -> None:
    pass

  @with_guild_lock
  async def clear(self, itx: discord.Interaction) -> None:
    _logger.debug("clearing")
    queue = self._get_queue(itx.guild)
    await self._dequeue(queue)

    await util.append_embed_to_interaction_response(
      itx,
      f"Cleared the queue",
      "",
      util.MAIN_COLOR
    )

  @with_guild_lock
  async def _get_details(self, itx: discord.Interaction) -> None:
    pass

  @with_guild_lock
  async def skip(self, itx: discord.Interaction) -> None:
    _logger.debug("skipping")
    queue = self._get_queue(itx.guild)
    current = queue[0]
    await self._player.stop_playing(itx.guild)

    await util.append_embed_to_interaction_response(
      itx,
      f"Skipped {current}",
      "",
      util.MAIN_COLOR
    )

  async def _fetch_title(self, item: QueueItem, link: str) -> None:
    try:
      item.title = util.get_youtube_title(link)
    except Exception as e:
      _logger.exception(e)
      await self._queue_event(
        QueueEvent(
          item.guild, item, QueueEventType.ERROR_RECOVERABLE, None, e
        )
      )

  @with_guild_lock
  async def queue(self, link: str, itx: discord.Interaction, stream: bool=False) -> None:
    item = QueueItem(link, itx=itx, guild=itx.guild, stream=stream) # type: ignore
    asyncio.Task(self._fetch_title(item, link))

    guild_queue = self._get_queue(item.guild)

    guild_queue.append(item)

    await util.append_to_interaction_response(itx, link)

    await self._queue_event(QueueEvent(item.guild, item, QueueEventType.ADDED))


  async def _queue_event(self, event: QueueEvent) -> None:
    await self._event_queue.put(event)
    _logger.debug(f"queued {event}")

  async def _dequeue(self, queue_items: list[QueueItem]|tuple[QueueItem]) -> None:
    """All the elements of the list must be related to the same Guild
    """

    guild = queue_items[0].guild
    guild_queue = self._get_queue(guild)

    for item in queue_items:
      try:
        index = guild_queue.index(item)
      except Exception as e:
        _logger.exception(e)
        continue

      _logger.debug("removing %r", item)
      guild_queue.remove(item)
      if item.fetch_task:
        item.fetch_task.cancel()

      if self._get_playing(guild) and index == 0:
        await self._player.stop_playing(guild)

    await self._prefetch_queue(guild)

  async def _event_queue_loop(self) -> None:
    _logger.debug("event queue loop started")
    while True:
      event = await self._event_queue.get()
      _logger.debug("processing %r", event)

      async with await self._get_guild_lock(event.guild):
        _logger.debug("queue: %r", pprint.pformat(self._get_queue(event.guild)))
        try:
          handler = self._handlers[event.type]
          await handler(event)
        except Exception as e:
          _logger.exception(e)

  async def _event_handler_finished_playing(self, event: QueueEvent) -> None:
    await self._dequeue((event.item,))
    queue = self._get_queue(event.item.guild)
    if queue:
      await self._play(queue[0])

  async def _event_handler_fetched(self, event: QueueEvent) -> None:
    await self._play(event.item)

  async def _event_handler_error_recoverable(self, event: QueueEvent) -> None:
    await util.append_to_interaction_response(
      event.item.itx,
      "\n" + str(event.exception),
      ":exclamation:Error",
      util.ERROR_COLOR
    )
    if event.action:
      _logger.debug("trying to recover from %r", event)
      await event.action()

  async def _event_handler_error_nonrecoverable(self, event: QueueEvent) -> None:
    await util.append_to_interaction_response(
      event.item.itx,
      "\n" + str(event.exception),
      ":exclamation:Error",
      util.ERROR_COLOR
    )
    await self._dequeue((event.item,))
    if event.item.fetch_task:
      event.item.fetch_task.cancel()
    if event.action:
      _logger.debug("launching action %r", event)
      await event.action()

  async def _event_handler_added(self, event: QueueEvent) -> None:
    if self._should_prefetch(event.item):
      await self._background_prefetch(event.item)
      return

    await self._play(event.item)

  async def _prefetch_queue(self, guild: discord.Guild) -> None:
    guild_queue = self._get_queue(guild)

    for item in guild_queue[:self._prefetch_item_count]:
      if self._should_prefetch(item):
        await self._background_prefetch(item)

  def _should_prefetch(self, item: QueueItem) -> bool:
    should: bool =  not item.stream and not item.file
    _logger.debug("should_prefetch: %r; item.stream %r; not item.file %r",
      should,
      not item.stream,
      not item.file
    )
    return should

  async def _background_prefetch(self, item: QueueItem) -> None:
    async def _fetch(item: QueueItem) -> None:
      try:
        item.file = await music.grabber.grab_to_file(item.link)
      except Exception as e:
        _logger.exception(e)
        await self._queue_event(
          QueueEvent(
            item.guild, item, QueueEventType.ERROR_NONRECOVERABLE, None, e
          )
        )
        return

      item.fetch_task = None
      _logger.debug(f"fetched {item.file.name} for {item.link}")
      await self._queue_event(QueueEvent(item.guild, item, QueueEventType.FETCHED))

    task = asyncio.Task(_fetch(item))
    item.fetch_task = task

  #TODO itx/it instead of author
  async def play_soundbit(self, soundbit_name: str, author: discord.User|discord.Member) -> None:
    if not await self._free_to_play(author.guild):
      return

    soundbit_file = soundbit_manager.get_soundbits()[soundbit_name]
    await self._player.play_file(soundbit_file, author, lambda e: _logger.error(e) if e else None)

  async def play_audiosource(self, source: discord.AudioSource, author: discord.User|discord.Member, callback: Callable[[Optional[Exception]], Any]) -> None:
    if not await self._free_to_play(author.guild):
      return

    await self._player.play_source(source, author, callback)

  async def _free_to_play(self, guild: discord.Guild) -> bool:
    async with await self._get_guild_lock(guild):
      if self._get_playing(guild):
        return False
      return True

  async def _play(self, item: QueueItem) -> None:
    #TODO and not paused
    if self._get_playing(item.guild) \
      or self._get_queue(item.guild).index(item) != 0:
      _logger.debug("not playing %r", item)
      return

    self._set_playing(item.guild, True)

    async def _play_end_async_callback(e: Exception|None) -> None:
      async with await self._get_guild_lock(item.guild):
        self._set_playing(item.guild, False)

      if e:
        _logger.error(e)
        return

      await self._queue_event(QueueEvent(item.guild, item, QueueEventType.FINISHED_PLAYING))

    event_loop = asyncio.get_event_loop()
    def _play_end_callback(e: Exception|None) -> None:
      event_loop.create_task(_play_end_async_callback(e))

    if item.stream:
      source = await YTDLSource.from_url(item.link, stream=True)
      await self._player.play_source(source, item.itx.user, _play_end_callback)
    else:
      await self._player.play_file(pathlib.Path(item.file.name), item.itx.user, _play_end_callback)

