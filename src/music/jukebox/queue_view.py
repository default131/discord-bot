import discord

import util.general as util


class QueueView():

  def get_view(self) -> discord.ui.View:
    """Returns a universal view of the jukebox. Buttons are persistent"""

    view = discord.ui.View(timeout=None)

    button_add = util.CustomButton(custom_id="jukebox_add", row=1, label="Add")
    button_add.set_callback(self._add)
    view.add_item(button_add)
    button_skip = util.CustomButton(custom_id="jukebox_skip", row=1, label="Skip")
    button_skip.set_callback(self._skip)
    view.add_item(button_skip)
    button_details = util.CustomButton(custom_id="jukebox_details", row=1, label="Get details")
    button_details.set_callback(self._get_details)
    view.add_item(button_details)
    button_clear = util.CustomButton(custom_id="jukebox_clear", row=1, label="Clear queue")
    button_clear.set_callback(self._clear)
    view.add_item(button_clear)

    return view