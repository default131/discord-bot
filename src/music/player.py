import pathlib
from typing import Any, Callable, Optional
import discord
from discord.ext import commands
import logging

_logger: logging.Logger = logging.getLogger(__name__)


class Player():
  def __init__(self, bot: commands.Bot) -> None:
    self._bot = bot

  async def play_file(self, file: pathlib.Path, author: discord.User|discord.Member, callback: Callable[[Optional[Exception]], Any]) -> None:
    audio_source = discord.FFmpegPCMAudio(file.as_posix())
    source = discord.PCMVolumeTransformer(audio_source)

    await self.play_source(source, author, callback)

  async def play_source(self, source: discord.AudioSource, author: discord.User|discord.Member, callback: Callable[[Optional[Exception]], Any]) -> None:
    voice_client = await self._prepare_voice_client(author)

    if voice_client.is_playing():
      voice_client.stop()
    voice_client.play(source, after=callback)

  def is_paused(self, guild: discord.Guild) -> bool:
    voice_client = Player.get_bot_voice_client(self._bot, guild)
    if voice_client == None:
      raise Exception("Could not get the voice connection")
    return voice_client.is_paused()

  async def stop_playing(self, guild: discord.Guild|None) -> None:
    if not guild:
      raise Exception("No guild provided")

    voice_client = Player.get_bot_voice_client(self._bot, guild)
    if voice_client == None:
      raise Exception("Could not get the voice connection")
    voice_client.stop()

  @staticmethod
  def _ensure_member(author: discord.User|discord.Member) -> None:
    _logger.debug(f"author: {author}")
    _logger.debug(f"author type: {type(author)}")

    if not issubclass(type(author), discord.Member):
      raise TypeError("Author does not belong to a server")

  async def _prepare_voice_client(self, author: discord.User|discord.Member) -> discord.VoiceClient:
    """Returns a bot's voice client connected to the authors voice channel,
      if the bot was not connected to any voice channel to begin with"""

    #ensure proper type of author
    Player._ensure_member(author)
    member: discord.Member = author # type: ignore

    await self._ensure_voice(member)
    voice_client = Player.get_bot_voice_client(self._bot, member.guild)

    #if for some reason there is still no voice client after connecting to a channel
    if not voice_client or not issubclass(type(voice_client), discord.VoiceClient):
      raise commands.CommandError("Could not get a voice client")

    return voice_client

  async def _ensure_voice(self, author: discord.Member) -> None:
    """Ensure that there is a voice client connected to a channel,
      or connects to the author's voice channel"""

    voice_client = Player.get_bot_voice_client(self._bot, author.guild)

    #if connected to a voice channel
    if issubclass(type(voice_client), discord.VoiceClient):
      return

    #if author is not connected to a void channel
    if not author.voice or not author.voice.channel:
      _logger.warning("Author not connected to a voice channel")
      raise commands.CommandError("Author not connected to a voice channel")

    await author.voice.channel.connect()

  @staticmethod
  def get_bot_voice_client(bot: commands.Bot, guild: discord.Guild) -> discord.VoiceClient|None:
    return next(
      (
        client for client
        in bot.voice_clients
        if guild == client.guild # type: ignore
      ),
      None
    )