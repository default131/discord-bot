import asyncio
import logging
import io
import subprocess
import discord
import sys

_logger: logging.Logger = logging.getLogger(__name__)

@staticmethod
async def get_tts_audiosource(text: str) -> discord.AudioSource:
  bytes = await get_tts_output(text)
  bytes_io = io.BytesIO(bytes)
  return discord.FFmpegOpusAudio(bytes_io, pipe=True)


@staticmethod
async def get_tts_output(text: str) -> bytes:
  _logger.debug("rendering TTS for: %s", text)

  command = [
    "piper/piper",
    "--model", "piper/pl_PL-gosia-medium.onnx",
    "--output_file", "-"
  ]
  _logger.debug(f"command: {" ".join(command)}")

  proc = await asyncio.create_subprocess_exec(
    *command,
    stdin=subprocess.PIPE,
    stderr=subprocess.PIPE,
    stdout=subprocess.PIPE
  )

  stdout, stderr = await proc.communicate(text.encode(sys.getdefaultencoding()))
  stderr_str: str = stderr.decode(sys.getdefaultencoding())

  if proc.returncode != 0:
    _logger.error(stderr_str)
    raise Exception(f"Could not generate TTS: {stderr_str}")


  return stdout