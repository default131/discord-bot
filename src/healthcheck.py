import socket
import threading
import os

import bot

class Healthcheck():
  def __init__(self, bot: bot.TheBot) -> None:
    self._socket_file = '/tmp/healthcheck.sock'
    self._bot = bot

  def start(self) -> None:
    if os.path.exists(self._socket_file):
      os.remove(self._socket_file)

    server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    server.bind(self._socket_file)
    server.listen(1)

    def _serve() -> None:
      while True:
        conn, _ = server.accept()
        with conn:
          if self._bot.is_healthy():
            conn.sendall(b'OK')
          else:
            conn.sendall(b'UNHEALTHY')

    threading.Thread(target=_serve).start()

