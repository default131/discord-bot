import asyncio
from asyncio import StreamReader, subprocess
from dataclasses import dataclass
import sys
from tempfile import NamedTemporaryFile
from typing import Any, Final, Optional
import logging

import yt_dlp

import util.general as util
from util.diskfile import DiskFile

_logger: logging.Logger = logging.getLogger(__name__)

class FormatException(Exception):
  ...

class ExtensionException(Exception):
  ...

UNIVERSAL_YTDL_OPTS: Final[dict[str,Any]] = {
  'restrictfilenames': True,
  'noplaylist': True,
  'nocheckcertificate': True,
  'ignoreerrors': False,
  'logtostderr': True,
  'quiet': False,
  'no_warnings': False,
  'default_search': 'auto',
  # bind to ipv4 since ipv6 addresses cause issues sometimes
  'source_address': '0.0.0.0',
}

async def get_yt_info(url: str, opts: dict|None = None, loop = None) -> dict:
  with yt_dlp.YoutubeDL(opts or UNIVERSAL_YTDL_OPTS) as ytdl:
    loop = loop or asyncio.get_event_loop()
    return await loop.run_in_executor(
      None,
      lambda: ytdl.sanitize_info(ytdl.extract_info(url, download=False))
    )

@dataclass
class MediaFormat:
  """A youtube video's format"""
  format_id: str
  audio: bool
  representation: str
  protocol: str
  codec: Optional[str] = None
  ext: Optional[str] = None
  size_B: Optional[int] = None
  resolution: Optional[str] = None
  fps: Optional[int] = None

  @staticmethod
  def from_json(format: dict[str, Any], duration_s: int) -> Any:
    try:
      return MediaFormat._from_json(format, duration_s)
    except Exception as e:
      _logger.exception(e)
      _logger.warning(format)

  @staticmethod
  def _from_json(format: dict[str, Any], duration_s: int) -> Any:
    def get(key: str) -> Any:
      return format.get(key, None)

    audio: bool = bool(get("audio_ext") and get("audio_ext") != "none")
    video: bool = bool(get("video_ext") and get("video_ext") != "none")

    if not audio and not video:
      _logger.debug("no audio and no video for %r", format)
      return None

    size_B: Optional[int] = None
    try:
      size_B = int(get("filesize"))
    except:
      pass
    try:
      size_B = round(int(get("tbr")) * 1000 * duration_s / 8)
    except:
      pass

    #args not in an unrolled dict for type checking and refactoring benefit
    if audio:
      return MediaFormat(
        format_id=get("format_id"),
        size_B=size_B,
        audio=audio,
        ext=get("ext"),
        codec=get("acodec"),
        representation=get("format"),
        protocol=get("protocol")
      )

    if video:
      fps: Optional[int] = None
      try:
        fps=int(get("fps"))
      except:
        pass

      return MediaFormat(
        format_id=get("format_id"),
        size_B=size_B,
        audio=audio,
        ext=get("ext"),
        codec=get("vcodec"),
        representation=get("format"),
        protocol=get("protocol"),
        resolution=get("resolution"),
        fps=fps
      )

    #not a media format
    return None

  def __str__(self) -> str:
    fields = [
      f"{round(self.size_B / util.Mi, 2)}MiB" if self.size_B else "",
      f"{self.fps}" if self.fps else "",
      f"{self.resolution}" if self.resolution else "",
      f"{self.codec}" if self.codec else "",
      f"{self.ext}" if self.ext else ""
    ]
    return " | ".join(fields)

  def __repr__(self) -> str:
    return self.format_id

class _CommandLineBuilder():
  def __init__(self, url: str) -> None:
    self._url = url
    #It is permitted to set only video or only audio
    self._format_video: Optional[MediaFormat] = None
    self._format_audio: Optional[MediaFormat] = None
    self._cut_points: Optional[tuple[str, str]] = None
    self._stream: bool = False
    self._ext: str|None = None

  def set_formats(self, format_video: MediaFormat|None=None, format_audio: MediaFormat|None=None) -> None:
    if not format_video and not format_audio:
      raise FormatException("No format provided")

    self._format_video = format_video or self._format_video
    self._format_audio = format_audio or self._format_audio

  @staticmethod
  def _deduct_ext(format_video: MediaFormat, format_audio: MediaFormat) -> str:
    if format_video.audio or not format_audio.audio:
      raise FormatException("Erroneus formats chosen")

    if not format_video.ext or not format_audio.ext:
      raise FormatException(f"No extension assinged to one of the formats: video={format_video.ext or 'none'} and audio={format_audio.ext or 'none'}")

    if format_video.ext == "mp4" and format_audio.ext == "m4a":
      return "mp4"
    if format_video.ext == "webm" and format_audio.ext == "webm":
      return "webm"

    raise FormatException(f"Non-matching formats chosen video={format_video.ext} and audio={format_audio.ext}")

  @staticmethod
  def _build_format_str(format_video: MediaFormat|None, format_audio: MediaFormat|None, cutting: bool=False) -> tuple[str, str|None]:
    """Returns the format spec string and the extension string. The extension string is only returned
    when it can be inferred from the format spec
    """

    audio_id = format_audio and format_audio.format_id
    video_id = format_video and format_video.format_id
    if video_id and audio_id:
      return video_id + "+" + audio_id, None
    elif video_id or audio_id:
      return video_id or audio_id, None #type: ignore
    else:
      _logger.debug("No format specified")

      last_resort_fallbacks = "/".join(
        [
          "bestvideo+bestaudio",
          "bestvideo",
          "bestaudio",
          "best[height<=1080]",
          "best"
        ]
      )

      if cutting:
        #seems that setting the protocol to https instead
        #of m3u8 significantly speeds up downloading sections?
        #
        #will not use vp9, downloading sections does things to keyframes
        #that i don't understand. There's no video for a few seconds before
        #the specified cut point. Forcing keyframes at cut points fixes this
        #but causes reencoding in software; don't know how to make yt-dlp use hardware with ffmpeg
        spec = "bestvideo[height<=1080][ext=mp4][vcodec*=avc][protocol=https]+bestaudio[ext=m4a][protocol=https]"
        ext = "mp4"
      else:
        spec = "/".join(
          [
            #on YT this will be VP9; HEVC download not supported by YT
            "bestvideo[height<=1080][ext=webm][protocol=https]+bestaudio[ext=webm][protocol=https]",
            "bestvideo[height<=1080][ext=mp4][vcodec*=avc][protocol=https]+bestaudio[ext=m4a][protocol=https]",
            #any mp4 as fallback, even AV1
            "bestvideo[height<=1080][ext=mp4][protocol=https]+bestaudio[ext=m4a][protocol=https]",
          ]
        )
        ext = None

      return spec + "/" + last_resort_fallbacks, ext

  async def _check_ext(self, url: str, format_spec: str) -> str:
    """Queries YT to check what the extension/container
      will be for the format spec
    """

    _logger.debug("checking extension for %s with format spec %s", url, format_spec)

    opts_with_format = UNIVERSAL_YTDL_OPTS.copy()
    opts_with_format["format"] = format_spec
    sanitized_information = await get_yt_info(url, opts_with_format)
    return sanitized_information["ext"]

  def set_cut_points(self, cut_point_start: str, cut_point_end: str) -> None:
    self._cut_points = (cut_point_start, cut_point_end)

  def set_stream(self, stream: bool) -> None:
    self._stream = stream

  def get_ext(self) -> str:
    if not self._ext:
      raise ExtensionException("The command has not been built yet; no extension set")
    return self._ext

  async def build(self) -> tuple[list[Any], Optional[DiskFile]]:
    command = ["yt-dlp", "--no-playlist", self._url]

    #cut points
    if self._cut_points:
      timestamp_start_s = _CommandLineBuilder._parse_timestamp(self._cut_points[0])
      timestamp_end_s = _CommandLineBuilder._parse_timestamp(self._cut_points[1])
      command += [
        #"--force-keyframes-at-cuts",
        "--download-sections",
        f'*{timestamp_start_s}-{timestamp_end_s}'
      ]

    #format spec
    format_spec, self._ext = _CommandLineBuilder._build_format_str(
      self._format_video,
      self._format_audio,
      bool(self._cut_points)
    )
    command += ["-f", format_spec]

    #extension/container
    if not self._ext:
      self._ext = await self._check_ext(self._url, format_spec)

    #output type, name
    output_file: Optional[DiskFile] = None
    if self._stream:
      command += ["-o", "-"]
    else:
      output_file = NamedTemporaryFile(suffix=f".{self._ext}")
      command += ["--force-overwrites", "-o", output_file.name]

    _logger.info("yt-dlp command: %r", command)
    return command, output_file

  @staticmethod
  def _parse_timestamp(timestamp: str) -> float:
    values: list[str] = timestamp.split(':')
    values.reverse()

    if not values:
        raise Exception("Could not parse timestamp; example timestamp: 12:04:12.1")

    seconds: float = float(values[0])
    minutes:int = 0
    hours:int = 0
    if len(values) > 1:
      minutes = int(values[1])
    if len(values) > 2:
      hours = int(values[2])

    return seconds + minutes * 60 + hours * 3600

class YTDLPWrapper():
  def __init__(self, url: str) -> None:
    self._url = url
    self._command_builder = _CommandLineBuilder(url)

  @staticmethod
  async def get_formats(url: str) -> tuple[list[MediaFormat], list[MediaFormat]]:
    """Returns (audio_formats, video_formats)
    """

    sanitized_information = await get_yt_info(url)

    duration = int(sanitized_information["duration"])

    media_formats: list[MediaFormat] = list(filter(None, [
      MediaFormat.from_json(format, duration)
      for format in sanitized_information["formats"]
    ]))
    audio_formats = list(filter(lambda format: format.audio, media_formats))
    audio_formats.sort(key=lambda format: format.size_B or 0)

    video_formats = list(filter(lambda format: not format.audio, media_formats))
    video_formats.sort(key=lambda format: format.size_B or 0)

    return (audio_formats, video_formats)

  def set_formats(self, format_video: MediaFormat, format_audio: MediaFormat) -> None:
    self._command_builder.set_formats(format_video, format_audio)

  def set_cut_points(self, cut_point_start: str, cut_point_end: str) -> None:
    self._command_builder.set_cut_points(cut_point_start, cut_point_end)

  async def download(self) -> DiskFile:
    command, disk_file = await self._command_builder.build()

    proc = await asyncio.create_subprocess_exec(*command, stderr=subprocess.PIPE)

    stderr: str = (await proc.communicate())[1].decode(sys.getdefaultencoding())

    if proc.returncode != 0:
      raise Exception(f"Could not get video: {stderr}")

    return disk_file #type: ignore

  async def download_stream(self) -> StreamReader:
    self._command_builder.set_stream(True)
    command, _ = await self._command_builder.build()

    proc = await asyncio.create_subprocess_exec(*command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # TODO error handling
    #stderr: str = (await proc.communicate())[1].decode(sys.getdefaultencoding())

    #if proc.returncode != 0:
    #  raise Exception(f"Could not get video: {stderr}")

    return proc.stdout #type: ignore
